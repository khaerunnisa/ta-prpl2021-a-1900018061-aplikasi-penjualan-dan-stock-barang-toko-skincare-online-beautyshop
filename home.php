<!DOCTYPE html>
<html>
<head>
	<title>web </title>
<style type="text/css">
body{
    font-family: arial;
    font-size: 14px;
    

}

#canvas{
	width: 960px;
	margin: 0 auto;
    border: 1px solid silver;
}

#header{
    font-size: 40px;
	padding: 20px;
	background-color: #660099;
	color: #fff;
	text-align: center;
	font-family: 'Marck Script', cursive;
}

#menu{
	background-color: #9966FF;
	text-align: center;
}
#menu ul{
	list-style: none;
	margin: 0;
	padding: 0;
	font-size: 24px;
}
#menu ul li.utama{
	display: inline-table;
}
#menu ul li:hover{
    background-color: #CCCCFF;
}
#menu ul li a{
	display: block;
	text-decoration: none;
	line height: 60px;
	padding: 30px;
	color: #fff;
}
.utama ul{
	display: none;
	position: absolute;
	z-index: 1px;
}
.utama:hover ul{
	display: block;
}
.utama ul li{
	display: block;
	background-color: #9933FF;
	width: 140px;
}
#isi{
    min-height: 420px;
    padding: 50px;
	background-image: url('pelanggan/images/gambar.jpg');
	background-size: 490px;
}

#footer{
    text-align: center;
    padding: 20px;
	background-color: #ccc;
}
</style>
</head>
<body>
<div id="canvas">
    <div id="header">
	SELAMAT DATANG DI BEAUTYSHOP
    </div> 

     <div id="menu">
	       <ul>
		   	<li class="utama"><a href="login.php">Login admin</a>
			   <ul>
					<li><a href="login.php">Masuk</a></li>
					<li><a href="">Daftar</a></li>
				</ul>
			</li>
			<li class="utama"><a href="pelanggan/login.php">Login pelanggan</a>
				<ul>
					<li><a href="">Masuk</a></li>
					<li><a href="">Daftar</a></li>
			   </ul>
			</li>
		   </ul>
    </div> 

     <div id="isi">
	 
    </div> 

     <div id="footer">
	 copyright 2021-BEAUTYSHOP
    </div> 
</div>
</body>
</html>