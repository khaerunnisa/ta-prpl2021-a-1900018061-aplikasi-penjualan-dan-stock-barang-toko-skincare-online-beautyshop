<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>produk!</title>
  </head>
  <body>
   

    <div class="container">
     <h2>Menampilkan Produk Toko Online</h2>
       <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <img src="pelanggan/images/avoskin.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title m-1">AVOSKIN</h5>
                        <p class="card-text">Avoskin PHTE.</p>
                        <a href="#" class="btn btn-primary">RP.90.000</a>
                        <a href="#" class="btn btn-success">UPDATE</a>
                        <a href="#" class="btn btn-success">DELETE</a>
                </div>
                </div>  
            </div>

        <div class="col-md-3">
                <div class="card">
                    <img src="pelanggan/images/senka.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title m-1">SENKA</h5>
                        <p class="card-text">Facial Foam.</p>
                        <a href="#" class="btn btn-primary">RP.30.000</a>
                        <a href="#" class="btn btn-success">UPDATE</a>
                        <a href="#" class="btn btn-success">DELETE</a>
                </div>
                </div>  
            </div>

        <div class="col-md-3">
                <div class="card">
                    <img src="pelanggan/images/SK22.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title m-1">SK II</h5>
                        <p class="card-text">Facial Tretment.</p>
                        <a href="#" class="btn btn-primary">RP.99.000</a>
                        <a href="#" class="btn btn-success">UPDATE</a>
                        <a href="#" class="btn btn-success">DELETE</a>
                </div>
                </div>  
            </div>
        <div class="col-md-3">
                <div class="card">
                    <img src="pelanggan/images/facemist.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title m-1">Face Mist</h5>
                        <p class="card-text">safron facemist.</p>
                        <a href="#" class="btn btn-primary">RP.70.000</a>
                        <a href="#" class="btn btn-success">UPDATE</a>
                        <a href="#" class="btn btn-success">DELETE</a>
                </div>
                </div>  
            </div>
        <div class="col-md-3">
                <div class="card">
                    <img src="pelanggan/images/laneige.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title m-1">Laneige</h5>
                        <p class="card-text">sleeing Mask.</p>
                        <a href="#" class="btn btn-primary">RP.99.000</a>
                        <a href="#" class="btn btn-success">UPDATE</a>
                        <a href="#" class="btn btn-success">DELETE</a>
                </div>
                </div>  
            </div>
        <div class="col-md-3">
                <div class="card">
                    <img src="pelanggan/images/wardahc.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title m-1">Wardah Serum</h5>
                        <p class="card-text">wardah defense serum.</p>
                        <a href="#" class="btn btn-primary">RP.75000</a>
                        <a href="#" class="btn btn-success">UPDATE</a>
                        <a href="#" class="btn btn-success">DELETE</a>
                </div>
                </div>  
            </div>
        <div class="col-md-3">
                <div class="card">
                    <img src="pelanggan/images/aloe.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title m-1">Nature Republic</h5>
                        <p class="card-text">Aloe Vera .</p>
                        <a href="#" class="btn btn-primary">RP.90.000</a>
                        <a href="#" class="btn btn-success">UPDATE</a>
                        <a href="#" class="btn btn-success">DELETE</a>
                </div>
                </div>  
            </div>


        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    -->
  </body>
</html>
