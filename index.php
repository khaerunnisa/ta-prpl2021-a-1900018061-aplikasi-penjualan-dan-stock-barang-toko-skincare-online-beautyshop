<?php 

	require_once("config.php");

	$sql_get = "SELECT * FROM barang";
	$query_brng = mysqli_query($koneksi, $sql_get);

	$results = [];

	while ($row = mysqli_fetch_assoc($query_brng)) {
		$results[] = $row;
	}


?> 

<!DOCTYPE html>
<html>
<head>
	<title>Daftar Barang</title>
</head>
<body>
	<h1>Daftar Barang</h1>

	<table border="1">
		<tr>
			<td>No</td>
			<td>Id Barang</td>
			<td>Nama Barang</td>
			<td>Stok</td>
			<td>Harga </td>
			<td>Aksi</td>
		</tr>
		<?php 
		$no = 1;
		foreach ($results as $result) :
		?>
		<tr>
			<td> <?= $no; ?> </td>
			<td> <?= $result['Id']; ?> </td>
			<td> <?= $result['nama_barang']; ?> </td>
			<td> <?= $result['stok']; ?> </td>
			<td> <?= $result['harga']; ?> </td>
			<td>
				<a href="update.php?id=<?=$result['Id'];?>">update</a>
				||
				<a href="hapus.php?id=<?=$result['Id'];?>">Hapus</a>
				
			</td>
		</tr>

		<?php 
		$no++;
		endforeach;
		?>
	</table>
	<br>
	<a href="tambah.php">Tambah Data</a>
	||
	<a href="history.php">History</a>
</body>
</html>
 