<?php 
	require_once("../config.php");

	$sql_get = "SELECT * FROM barang";
	$query_brng = mysqli_query($koneksi, $sql_get);

	$results = [];

	while ($row = mysqli_fetch_assoc($query_brng)) {
		$results[] = $row;
	}


?> 

<!DOCTYPE html>
<html>
<head>
	<title>Daftar Barang</title>

</head>
<body>
        <h2>Daftar Barang</h2>
	<table border= "1">
		<tr>
			<td>No</td>
			<td>Id Barang</td>
			<td>Nama Barang</td>
			<td>Stok</td>
			<td>Harga </td>
			<td>Aksi</td>
		</tr>
		<?php 
		$no = 1;
		foreach ($results as $result) :	
		?>
		<tr>
			<td> <?= $no; ?> </td>
			<td> <?= $result['Id']; ?> </td>
			<td> <?= $result['nama_barang']; ?> </td>
			<td> <?= $result['stok']; ?> </td>
			<td> <?= $result['harga']; ?> </td>
			<td>
							<a href="beli.php?id_barang=<?=$result['Id'];?>">beli</a>

			</td>
		</tr>

		<?php 
		$no++;
		endforeach;
		?>
	</table>
	<br>
    
</body>
</html>
